# std lib
from os import getenv
import time
import random
import threading
from datetime import datetime, timedelta
import urllib.parse

# deps
import yaml
import requests
from rich import print  # type: ignore
from dateutil import parser

with open(getenv("AOTW_CONFIG_PATH", "config.yaml")) as f:
    config = yaml.safe_load(f)

print(config)

assert "matrix" in config
assert "order" in config
order = config["order"]
assert "username" in config["matrix"]
username = config["matrix"]["username"]
assert "password" in config["matrix"]
password = config["matrix"]["password"]
assert "homeserver" in config["matrix"]
homeserver = config["matrix"]["homeserver"]
assert "chatroomalias" in config["matrix"]
chatroomalias = config["matrix"]["chatroomalias"]
assert "queueroomalias" in config["matrix"]
queueroomalias = config["matrix"]["queueroomalias"]

event_type = "email.internetpost.aotw"


r1 = requests.get(
    f"{homeserver}/_matrix/client/v3/directory/room/{urllib.parse.quote(queueroomalias, safe='')}"
)
queueroomid = r1.json()["room_id"]
print(queueroomid)

r2 = requests.get(
    f"{homeserver}/_matrix/client/v3/directory/room/{urllib.parse.quote(chatroomalias, safe='')}"
)
chatroomid = r2.json()["room_id"]
print(chatroomid)


def login(homeserver, username, password):
    """Log in to Matrix homeserver"""
    # get access token
    r = requests.post(
        f"{homeserver}/_matrix/client/r0/login",
        json={
            "type": "m.login.password",
            "identifier": {"type": "m.id.user", "user": username},
            "password": password,
        },
    )
    if not r.ok:
        print(f"{r.status_code} on login")
        exit(1)
    return r.json()["access_token"]


def join_room(homeserver, access_token, chatroomalias):
    """Join Matrix room"""
    r = requests.post(
        f"{homeserver}/_matrix/client/r0/join/{requests.utils.quote(chatroomalias)}",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    if not r.ok:
        print(f"{r.status_code} on join: {r.json()}")
        exit(1)
    return r.json()["room_id"]


def send_message(homeserver, access_token, room_id, message, formatted_body=None):
    txn_id = random.randint(0, 1_000_000_000)
    event_content = {"msgtype": "m.text", "body": message} | (
        {"format": "org.matrix.custom.html", "formatted_body": formatted_body}
        if formatted_body
        else {}
    )
    r = requests.put(
        f"{homeserver}/_matrix/client/r0/rooms/{room_id}/send/m.room.message/{txn_id}",
        headers={"Authorization": f"Bearer {access_token}"},
        json=event_content,
    )
    if not r.ok:
        print(f"{r.status_code} while sending message: {r.json()}")
        exit(1)


def get_album(week_number, access_token):
    url = f"{homeserver}/_matrix/client/v3/rooms/{queueroomid}/state/{event_type}/{week_number}"
    r = requests.get(url, headers={"Authorization": f"Bearer {access_token}"})
    jdata = r.json()
    return jdata["content"], jdata["sender"], order[week_number % len(order)]


def post_album(homeserver, access_token, roomid, week_number=None):
    if week_number == None:
        week_number = datetime.now().isocalendar()[1]
        week_format = "This week's"
    else:
        week_format = f"Week {week_number}'s'"

    try:
        album_content, sender, next_person = get_album(week_number, access_token)
    except Exception as e:
        print(e)
        send_message(
            homeserver,
            access_token,
            roomid,
            f"Album not set for {week_number=}",
        )
        return

    # send message with formatted text
    msg = f"✨ {week_format} album was selected by {sender} ✨\n{album_content['body']}"
    formatted_msg = f"✨ {week_format} album was selected by <a href=\"https://matrix.to/#/{sender}\">{sender}</a> ✨<br>{album_content.get('formatted_body', album_content['body'])} "
    send_message(
        homeserver,
        access_token,
        roomid,
        msg,
        formatted_body=formatted_msg,
    )


def chat_worker(homeserver, access_token, roomid):
    """Wait until next monday, then post the appropriate album. Forever."""

    def seconds_until_next_monday():
        now = datetime.now()
        time_until_monday = timedelta(
            days=6 - now.weekday(),
            hours=23 - now.hour,
            minutes=59 - now.minute,
            seconds=70 - now.second,
        )
        return time_until_monday / timedelta(seconds=1)

    while True:
        now = datetime.now()
        next_week_number = now.isocalendar()[1] + 1
        time.sleep(seconds_until_next_monday())

        # time.sleep(10) #debug
        album_content, sender, next_person = get_album(next_week_number, access_token)

        # send message with formatted text
        msg = f"✨ This week's album was selected by {sender} ✨\n Next week: {next_person}\n{album_content['body']}"
        formatted_msg = f"✨ This week's album was selected by <a href=\"https://matrix.to/#/{sender}\">{sender}</a> ✨<br>Next week: <a href=\"https://matrix.to/#/{next_person}\">{next_person}</a> <br>{album_content.get('formatted_body', album_content['body'])} "
        send_message(
            homeserver,
            access_token,
            chatroomid,
            msg,
            formatted_body=formatted_msg,
        )


def sync(homeserver, access_token, since=None):
    r = requests.get(
        f"{homeserver}/_matrix/client/v3/sync",
        params={"timeout": 5000} | ({"since": since} if since else {}),
        headers={"Authorization": f"Bearer {access_token}"},
    )
    if not r.ok:
        print(f"{r.status_code} from sync.")
        exit(1)
    return r.json()


def sync_worker(homeserver, access_token):
    """Call Matrix /sync forever, react to events"""
    print("Starting sync worker...")
    since = None
    while True:

        if since is None:
            # get initial since token (and first events)
            # dont process events - we don't care about the past
            r = sync(homeserver, access_token)
            since = r["next_batch"]
            continue

        # call sync (runs after first loop)
        r = sync(homeserver, access_token, since=since)
        since = r["next_batch"]

        if "rooms" not in r:
            # print("No events in sync result")
            continue

        # join all invites
        for invite_roomid in r["rooms"].get("invite", []):
            print(f"Joining room {invite_roomid}")
            join_room(homeserver, access_token, invite_roomid)

        if "join" not in r["rooms"]:
            print("No events from joined rooms")
            continue

        # react to events in joined rooms
        for roomid, room in r["rooms"]["join"].items():
            for event in room["timeline"]["events"]:
                if event["type"] != "m.room.message":
                    # print("Skipping non-message event")
                    continue

                if event["content"]["msgtype"] != "m.text":
                    # print("Skipping non-text message")
                    continue

                # get text content and do lazy parse
                text_content = event["content"]["body"]
                if text_content.startswith("!repost"):
                    if (
                        len(text_content.split()) > 1
                        and (weekstr := text_content.split()[1]).isdigit()
                        and (weeknum := int(weekstr)) <= datetime.now().isocalendar()[1]
                    ):
                        post_album(
                            homeserver, access_token, roomid, week_number=weeknum
                        )
                    else:
                        post_album(homeserver, access_token, roomid)
                if (
                    text_content.startswith("!week ")
                    and len(text_content.split()) > 2
                    and (weekstr := text_content.split()[1]).isdigit()
                    and (weeknum := int(weekstr)) <= 53
                ):
                    # succesfully parsed a "!week" message
                    if event["sender"] == order[(weeknum - 1) % len(order)]:
                        # permissions are OK
                        msg = (
                            f"🎵 {event['sender']} just set an album for week {weeknum}"
                        )
                        formatted_msg = f"🎵 <a href=\"https://matrix.to/#/{event['sender']}\">{event['sender']}</a> just set an album for week {weeknum}"
                        print(msg)

                        # notify relevant rooms
                        send_message(
                            homeserver,
                            access_token,
                            chatroomid,
                            msg,
                            formatted_body=formatted_msg,
                        )
                        confirmation = f"You set an album for week {weeknum}."
                        send_message(homeserver, access_token, roomid, confirmation)

                        # strip "!week <weeknum> " from message
                        prefix = text_content.split()[0] + " " + text_content.split()[1]
                        event["content"]["body"] = (
                            event["content"]["body"].removeprefix(prefix).strip()
                        )
                        if "formatted_body" in event["content"]:
                            # strip from `formatted_body` if present
                            event["content"]["formatted_body"] = (
                                event["content"]["formatted_body"]
                                .removeprefix(prefix)
                                .strip()
                            )

                        # post stripped event to queue room
                        r = requests.put(
                            f"{homeserver}/_matrix/client/v3/rooms/{queueroomid}/state/{event_type}/{weeknum}",
                            headers={"Authorization": f"Bearer {access_token}"},
                            json=event,
                        )
                        if not r.ok:
                            print(f"Failed sending to queue room:", r.json())
                    else:
                        msg = f"⚠{event['sender']} just tried to change week {weeknum}'s album. Shame.. ⚠'"
                        formatted_msg = f"⚠<a href=\"https://matrix.to/#/{event['sender']}\">{event['sender']}</a> just tried to change week {weeknum}'s album. Shame.. ⚠'"
                        print(msg)

                        send_message(
                            homeserver,
                            access_token,
                            chatroomid,
                            msg,
                            formatted_body=formatted_msg,
                        )
                        error = f"You don't have permission to change week {weeknum}."
                        send_message(homeserver, access_token, roomid, error)
                else:
                    print(f"This text content does not parse:")
                    print(text_content)


if __name__ == "__main__":
    # do da matrix dance
    access_token = login(homeserver, username, password)
    roomid = join_room(homeserver, access_token, chatroomalias)

    # start worker to post configs from file
    poster = threading.Thread(
        target=chat_worker, args=(homeserver, access_token, chatroomalias)
    )
    poster.start()

    syncer = threading.Thread(target=sync_worker, args=(homeserver, access_token))
    syncer.start()

    # wait (forever) for threads
    poster.join()
    syncer.join()
