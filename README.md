# Album-of-the-week bot

Any user can schedule an album for any week. At 00:00 on monday in the given week, that album is posted in the room `chatroomalias`.

To schedule an album `album` for week `weeknumber`, message the bot in a non-encrypted room the following:

```
!week {weeknumber} {album}
```
